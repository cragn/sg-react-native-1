import React from 'react';
import { View, Button, Text, StyleSheet } from 'react-native';

const Square = ({ color, increase, decrease, value }) =>
    <View style={styles.vview}>
        <View
            style={{ ...styles.square, backgroundColor: color }}
        />
        <View style={styles.hview}>
            <Button
                title='+'
                onPress={increase}
            />
            <Button
                title='-'
                onPress={decrease}
            />
        </View>
        <View style={styles.hview}>
            <Text>Current value:</Text>
            <Text>{value}</Text>
        </View>
    </View>

const styles = StyleSheet.create({
    vview: {
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: "center"
    },
    hview: {
        flexDirection: 'column',
        marginLeft: 5
    },
    square: {
        width: 75,
        height: 75,
        marginRight: 5
    }
});

export default Square;