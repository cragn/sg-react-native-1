import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const ImageDetail = ({ title, imageSource }) =>
    <View style={styles.view}>
        <Image
            source={imageSource}
            style={styles.image}
        />
        <Text style={styles.title}>{title}</Text>
    </View>;

const styles = StyleSheet.create({
    view: {
        marginTop: 10
    },
    image: {
        width: '100%'
    },
    title: {
        fontSize: 20,
        color: 'white',
        backgroundColor: 'grey',
        padding: 5,
        textAlign: 'center'
    }
});

export default ImageDetail;