import React from 'react';
import { View } from 'react-native';

const Box = ({ bgColor, additionalStyle = {} }) =>
    <View style={{
        width: 50,
        height: 50,
        backgroundColor: bgColor || 'yellow',
        padding: 20,
        margin: 20,
        borderWidth: 20,
        borderColor: 'black',

        ...additionalStyle
    }} />;

export default Box;