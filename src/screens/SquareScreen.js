import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import Square from '../components/Square';

const ordering = ['red', 'green', 'blue'];

const checkValue = v => v > 255 ? 255 : v < 0 ? 0 : v;
const getColor = ({ red, green, blue }) => `rgb(${red}, ${green}, ${blue})`;

const SquareScreen = () => {
    const [color, setColor] = useState({
        red: Math.floor(Math.random() * 256),
        green: Math.floor(Math.random() * 256),
        blue: Math.floor(Math.random() * 256)
    });

    const increaseColor = c => setColor({ ...color, [c]: checkValue(color[c]+10) });
    const decreaseColor = c => setColor({ ...color, [c]: checkValue(color[c]-10) });

    return (
        <View>
            {ordering.map(c => <Square key={c} color={c} value={color[c]} increase={() => increaseColor(c)} decrease={() => decreaseColor(c)} />)}
            <View
                style={{
                    ...styles.result,
                    backgroundColor: getColor(color)
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    result: {
        marginTop: 20,
        width: '100%',
        height: '100%'
    }
});

export default SquareScreen;