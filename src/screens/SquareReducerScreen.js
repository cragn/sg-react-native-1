import React, { useReducer } from 'react';
import { View, StyleSheet } from 'react-native';
import Square from '../components/Square';

const checkValue = v => v > 255 ? 255 : v < 0 ? 0 : v;
const getColor = ({ red, green, blue }) => `rgb(${red}, ${green}, ${blue})`;

const reducer = (state, action) => {
    console.log('action: ');
    console.log(action);
    switch (action.type) {
        case 'increment':
            newState = { ...state };
            newState[action.color] = checkValue(state[action.color] + 10);
            return newState;
            // return {
            //     ...state,
            //     [action.color]: checkValue(state[action.color] + 10)
            // }; // not cool - ordering

        case 'decrement':
            newState = { ...state };
            newState[action.color] = checkValue(state[action.color] - 10);
            return newState;

        default:
            return state;
    }
};


const SquareScreen = () => {
    const initialState = {
        red: Math.floor(Math.random() * 256),
        green: Math.floor(Math.random() * 256),
        blue: Math.floor(Math.random() * 256)
    };
    
    const [state, dispatch] = useReducer(reducer, initialState);


    return (
        <View>
            {Object.keys(state).map(color => (
                <Square
                    key={color}
                    color={color}
                    value={state[color]}
                    increase={() => dispatch({ type: 'increment', color })}
                    decrease={() => dispatch({ type: 'decrement', color })}
                />
            ))}
            <View style={{
                ...styles.result,
                backgroundColor: getColor(state)
            }} />
        </View>
    )
};

const styles = StyleSheet.create({
    result: {
        marginTop: 20,
        width: '100%',
        height: '100%'
    }
});

export default SquareScreen;