import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const ComponentsScreen = () =>
    <View style={styles.viewStyle}>
        <Text style={styles.textComponent}>OMG</Text>
        <Text style={styles.secondText}>What's going on?</Text>
    </View>

const styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'yellow'
    },
    textComponent: {
        fontSize: 30
    },
    secondText: {
        fontSize: 13,
        color: 'red',
        textAlign: 'center'
    }
});

export default ComponentsScreen;