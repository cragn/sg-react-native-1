import React from 'react';
import { View, StyleSheet } from 'react-native';
import ImageDetail from '../components/ImageDetail';

const ImageScreen = () =>
    <View>
        <ImageDetail title='Beach' imageSource={require('../../assets/beach.jpg')} />
        <ImageDetail title='Forest' imageSource={require('../../assets/forest.jpg')} />
        <ImageDetail title='Mountain' imageSource={require('../../assets/mountain.jpg') }/>
    </View>;

const style = StyleSheet.create({});

export default ImageScreen;