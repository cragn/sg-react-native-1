import React, { useState } from 'react';
import { View, Button, Text, StyleSheet } from 'react-native';

const CounterScreen = () => {
    const [counter, setCounter] = useState(0);

    const increaseCounter = () => setCounter(counter +1);
    const decreaseCounter = () => setCounter(counter -1);

    return (
        <View>
            <Button 
                title='Increase counter'
                onPress={increaseCounter}
            />
            <Button
                title='Decrease counter'
                onPress={decreaseCounter}
            />
            <Text style={styles.currentText}>Current counter:</Text>
            <Text style={styles.counterText}>{counter}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    currentText: {
        fontSize: 24,
        textAlign: 'center'
    },
    counterText: {
        fontSize: 32,
        textAlign: 'center'
    }
});

export default CounterScreen;