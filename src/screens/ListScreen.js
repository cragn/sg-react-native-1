import React from 'react';
import { View, FlatList, Text, StyleSheet } from 'react-native';

const ListScreen = () => {
    const friends = [
        { name: 'Friend #1', age: 10, color: '#605203' },
        { name: 'Friend #2', age: 15, color: '#705203' },
        { name: 'Friend #3', age: 20, color: '#805203' },
        { name: 'Friend #4', age: 25, color: '#905203' },
        { name: 'Friend #5', age: 30, color: '#605203' },
        { name: 'Friend #6', age: 35, color: '#606203' },
        { name: 'Friend #7', age: 40, color: '#607203' },
        { name: 'Friend #8', age: 35, color: '#608203' },
        { name: 'Friend #9', age: 56, color: '#605903' },
        { name: 'Friend #10', age: 16, color: '#605293' },
        { name: 'Friend #11', age: 53, color: '#605263' }

    ];

    return (
        <View>
            <Text style={styles.styleTitle}>List Screen</Text>
            <FlatList
                // horizontal
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={true}
                keyExtractor={friend => friend.name}
                data={friends}
                renderItem={({item}) => <Text style={{ ...styles.listElement, backgroundColor: item.color }}>Name: {item.name}, Age: {item.age}</Text>}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    styleTitle: {
        textAlign: 'center',
        fontSize: 30
    },
    listElement: {
        textAlign: 'center',
        fontSize: 14,
        padding: 30,
        color: 'white'
    }
});

export default ListScreen;