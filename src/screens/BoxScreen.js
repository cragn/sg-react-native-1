import React from 'react';
import { View, StyleSheet } from 'react-native';
import Box from '../components/Box';

const BoxScreen = () =>
    <View>
        <View style={styles.row}>
            <Box bgColor='red' />
            <Box bgColor='blue' />
            <Box />
        </View>
        <View style={styles.row2}>
            <Box bgColor='green' />
            <Box bgColor='orange' additionalStyle={{
                position: 'absolute',
                left: 136.5,
                top: 60,
                zIndex: 6
            }} />
            <Box bgColor='violet' />
        </View>
    </View>;

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'green'
    },
    row2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'yellow'
    }
});

export default BoxScreen;