import React from 'react';
import { FlatList, Button, View, TouchableOpacity, Text, StyleSheet } from 'react-native';

const components = [
  'Components',
  'List',
  'Image',
  'Counter',
  'Color',
  'Square',
  'SquareReducer',
  'Input',
  'Box'
];


const HomeScreen = ({ navigation }) => {
  const renderButton = componentName =>
    <Button
      onPress={() => navigation.navigate(componentName)}
      title={`Go to ${componentName} Screen`}
    />;

  const renderTouchableOpacity = componentName =>
    <TouchableOpacity
      onPress={() => navigation.navigate(componentName)}
      style={styles.touchableButton}
    >
      <Text style={{
        textTransform: 'uppercase',
        textAlign: 'center'
      }}>{componentName}</Text>
    </TouchableOpacity>;

  return (
    <View style={{ backgroundColor: '#A9E5BB', width: '100%', height: '100%' }}>
      <View style={styles.topView}>
        <Text style={styles.topText}>Components: {components.length}</Text>
      </View>
      <View style={styles.componentsView}>
        <FlatList
          keyExtractor={component => component}
          data={components}
          renderItem={({ item }) => renderTouchableOpacity(item)}
        />
      </View>  
    </View>
  );
};

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#F72C25',
    padding: 7,
    borderWidth: 7,
    borderColor: '#F7B32B'
  },
  topText: {
    fontSize: 20,
    textAlign: 'center',
    color: '#2D1E2F',
    textTransform: 'uppercase'
  },
  touchableButton: {
    backgroundColor: '#FCF6B1',
    borderWidth: 4,
    borderColor: '#F7B32B',
    padding: 10,
    marginTop: 10
  },
  touchableButtonH: {
    backgroundColor: 'powderblue',
    padding: 10,
  }
});

export default HomeScreen;
