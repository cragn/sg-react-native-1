import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

const InputScreen = () => {
    const [text, onChangeText] = useState('');

    return (
        <View>
            <Text style={styles.text}>Enter password:</Text>
            <TextInput 
                style={styles.input}
                value={text} 
                onChangeText={t => onChangeText(t)}
                autoCapitalize='none'
                autoCorrect={false}
                secureTextEntry
            />
            { text
                ? text.length > 5
                    ? <Text style={styles.textGreen}>OK</Text>
                    : <Text style={styles.textRed}>Password must be longer than 5 chars.</Text>
                : null
            }
        </View>
    )
};

const styles = StyleSheet.create({
    text: {
        fontSize: 20
    },
    textGreen: {
        fontSize: 30,
        color: 'lime'
    },
    textRed: {
        fontSize: 30,
        color: 'red'
    },
    input: {
        padding: 7,
        margin: 10,
        fontSize: 32,
        borderColor: 'blue',
        borderWidth: 1
    }
});

export default InputScreen