import React, { useState } from 'react';
import { View, Button, FlatList, Text, StyleSheet } from 'react-native';

const ColorScreen = () => {
    const [colors, setColors] = useState([]);

    const randomValue = () => Math.floor(Math.random() * 256);
    const randomColor = () => `rgb(${randomValue()}, ${randomValue()}, ${randomValue()})`;

    return (
        <View>
            <Button
                title='Add color'
                onPress={() => {
                    setColors([
                        ...colors,
                        randomColor()
                    ]);  
                }}
            />
            <FlatList
                keyExtractor={color => color}
                data={colors}
                renderItem={({ item }) => <View style={{ ...styles.rect, backgroundColor: item }} />}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    rect: {
        width: '100%',
        height: 30,
        marginTop: 3
    }
});

export default ColorScreen;